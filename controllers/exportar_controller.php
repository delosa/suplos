<?php
    //Vista para los filtros necesarios para la exportacion
    require_once("../models/modelo.php");
    $bienes_raices = new BienesRaices();
    $bienes_raices_totales = json_decode(file_get_contents("../data-1.json"));

    $ciudades = array();
    $tipos = array();
    foreach ($bienes_raices_totales as $bienes) {
        if (!isset($ciudades[$bienes->Ciudad])) {
            $ciudades[$bienes->Ciudad] = 1;
        }
        if (!isset($tipos[$bienes->Tipo])) {
            $tipos[$bienes->Tipo] = 1;
        }
    }
    require_once("../views/vista-exportar.php");
?>