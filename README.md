# suplosBackEnd
Prueba suplos desarrollador backend

### Repositorio ###

* https://bitbucket.org/delosa/suplos/
* PHP Versión 7 o superior.

### Ejecutar el proyecto ###

* Clonar los archivos del repositorio de Bitbucket antes mencionado
* En la carpeta "bd" se encuentra el archivo .sql que se debe importar en la base de datos MySQL
* Con ese archivo se creará la BD y las tablas a utilizar.
* En la siguiente dirección de su proyecto models/modelo.php en la funcion __construct(), se deberá agregar el host a utilizar, y accesos a la base de datos con usuario y contraseña, despúes del nombre de la base de datos entre comillas.
* Nos dirigiremos al link de nuestros proyecto y ejecutaremos nuestra aplicación.