<?php

class BienesRaices {
    
    private $bienes_raices;
    private $db;

    public function __construct() {
        $this->bienes_raices = array();
        $this->db = new PDO('mysql:host=localhost;dbname=intelcost_bienes', "root", "");
    }

    public function getBienesRaices() {
        $sql = "SELECT * FROM bienes_guardados;";
        foreach ($this->db->query($sql) as $res) {
            $this->bienes_raices[] = $res;
        }
        return $this->bienes_raices;    
        $this->db = null;
    }

    public function listBienesRaicesBusq($ciudad, $tipo) {
        $bienes_raices = json_decode(file_get_contents("data-1.json"));

        foreach ($bienes_raices as $bienes) {
            if ($bienes->Ciudad == $ciudad && $bienes->Tipo == $tipo) {
                $busqueda[] = $bienes;
            }
        }
        $this->bienes_raices = $busqueda;
        return $this->bienes_raices;
    }


    public function setBienesRaices($direccion, $telefono, $ciudad, $tipo, $codigo_postal, $precio) {
        $sql = "INSERT INTO bienes_guardados(direccion, ciudad, telefono, codigo_postal, tipo, precio) 
                VALUES ('" . $direccion . "', '" . $ciudad . "', '" . $telefono . "', " . $codigo_postal . ", '" . $tipo . "', '" . $precio . "');";
        $result = $this->db->query($sql);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBienesRaices($id) {
        $sql = "DELETE FROM bienes_guardados WHERE id = ".$id.";";
        $result = $this->db->query($sql);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function exportarBienesRaicesBusq($ciudad, $tipo) {
        $this->bienes_raices = [];
        $sql = "SELECT * FROM bienes_guardados WHERE ciudad = '".$ciudad."' AND tipo = '".$tipo."';";
        foreach ($this->db->query($sql) as $res) {
            $this->bienes_raices[] = $res;
        }
        return $this->bienes_raices;    
        $this->db = null;
    }
}
?>