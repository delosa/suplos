
<!DOCTYPE html>
<?php 
include "models/modelo.php";
$bienesraices_guardar = new BienesRaices();

//Listado de Todos los bienes
if ((isset($_POST['ciudad_busq'])) && ($_POST['ciudad_busq'] != '') && (isset($_POST['tipo_busq'])) && ($_POST['tipo_busq'] != '')) {
    $listado = $bienesraices_guardar->listBienesRaicesBusq($_POST['ciudad_busq'], $_POST['tipo_busq']);
}

//Listado de la busqueda de los bienes por tipos y ciudades
if ((isset($_POST['ciudad'])) && ($_POST['ciudad'] != '') && (isset($_POST['tipo'])) && ($_POST['tipo'] != '') && $_POST['direccion'] && ($_POST['direccion'] != '') && (isset($_POST['telefono'])) && ($_POST['telefono'] != '') && $_POST['codigo_postal'] && ($_POST['codigo_postal'] != '') && (isset($_POST['precio']) && ($_POST['precio'] != ''))) {    
    $insercion = $bienesraices_guardar->setBienesRaices($_POST['direccion'], $_POST['telefono'], $_POST['ciudad'], $_POST['tipo'], $_POST['codigo_postal'], $_POST['precio']);
    $listado = $bienesraices_guardar->listBienesRaicesBusq($_POST['ciudad'], $_POST['tipo']);
    $ciudad_busq = $_POST['ciudad'];
    $tipo_busq = $_POST['tipo'];
    $_POST = [];
    $_POST['ciudad_busq'] = $ciudad_busq;
    $_POST['tipo_busq'] = $tipo_busq;
}

//Se eliminan los bienes guardados
if((isset($_POST['id'])) && ($_POST['id'] != '')){   
    $delete = $bienesraices_guardar->deleteBienesRaices($_POST['id']);
}

//Funcion para exportar
if($_POST['exportar'] == 1){
    $exportar = $bienesraices_guardar->exportarBienesRaicesBusq($_POST['ciudad'], $_POST['tipo']);
    require_once("views/exportar.php");
}

$bienes_raices = json_decode(file_get_contents("data-1.json"));
//Se obtienen las ciudades y tipo segun las cantidades y opciones que hayan
$ciudades = array();
$tipos = array();
foreach ($bienes_raices as $bienes) {
    if (!isset($ciudades[$bienes->Ciudad])) {
        $ciudades[$bienes->Ciudad] = 1;
    }
    if (!isset($tipos[$bienes->Tipo])) {
        $tipos[$bienes->Tipo] = 1;
    }
}
?>
<html>
<head>
  <meta charset="utf-8">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="css/customColors.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="css/ion.rangeSlider.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css"  media="screen,projection"/>
  <link type="text/css" rel="stylesheet" href="css/index.css"  media="screen,projection"/>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Formulario</title>
</head>

<body>
  <video src="img/video.mp4" id="vidFondo"></video>

  <div class="contenedor">
    <div class="card rowTitulo">
      <h1>Bienes Intelcost</h1>
    </div>
    <div class="colFiltros">
      <form action="#" method="post" id="formulario">
        <div class="filtrosContenido">
          <div class="tituloFiltros">
            <h5>Filtros</h5>
          </div>
          <div class="filtroCiudad input-field">
            <p><label for="selectCiudad">Ciudad:</label><br></p>
            <select name="ciudad_busq" id="selectCiudad">
              <option value="">Elige una ciudad</option>
              <?php
                foreach($ciudades as $ciudad => $value){
                    if($_POST['ciudad_busq'] && $ciudad == $_POST['ciudad_busq']){ ?>
                        <option value="<?php echo $ciudad;?>" selected><?php echo $ciudad;?></option>
                        <?php
                    }
                    else{ ?>
                        <option value="<?php echo $ciudad;?>"><?php echo $ciudad;?></option>
                    <?php
                    }
                    ?>
                    <?php
                }
              ?>
            </select>
          </div>
          <div class="filtroTipo input-field">
            <p><label for="selecTipo">Tipo:</label></p>
            <br>
            <select name="tipo_busq" id="selectTipo">
              <option value="">Elige un tipo</option>
              <?php
                foreach($tipos as $tipo => $value){
                    if($_POST['tipo_busq'] && $tipo == $_POST['tipo_busq']){ ?>
                        <option value="<?php echo $tipo;?>" selected><?php echo $tipo;?></option>
                        <?php
                    }
                    else{ ?>
                        <option value="<?php echo $tipo;?>"><?php echo $tipo;?></option>
                    <?php
                    }
                    ?>
                    <?php
                }
              ?>
            </select>
          </div>
          <div class="filtroPrecio">
            <label for="rangoPrecio">Precio:</label>
            <input type="text" id="rangoPrecio" name="precio" value="" />
          </div>
          <div class="botonField">
            <input type="submit" class="btn white" value="Buscar" id="submitButton">
          </div>
        </div>
      </form>
    </div>
    <div id="tabs" style="width: 75%;">
      <ul>
        <li><a href="#tabs-1">Bienes disponibles</a></li>
        <li><a href="controllers/mis_bienes.php">Mis bienes</a></li>
        <li><a href="controllers/exportar_controller.php">Reportes</a></li>
      </ul>
      <div id="tabs-1">
        <div class="colContenido" id="divResultadosBusqueda">
          <div class="tituloContenido card" style="justify-content: center;">
            <?php
                if(isset($listado)){
                    $busq = count($listado);
                }
            ?>
            <h5>Resultados de la búsqueda: <?php echo $busq; ?></h5>
            <div class="divider"></div>
            <?php
            if(isset($listado)){
                $bienes_mostrar = $listado;
            }
            else{
                $bienes_mostrar = $bienes_raices;
            }?>
            
            <?php
            //Vista de Todos los bienes y los bienes por busqueda
            foreach ($bienes_mostrar as $bienes){?>
                <div class="bienes_raices">
                    <form action="#" method="post" id="<?php echo $bienes->Id; ?>">
                        <img src="img/casas.png" width="250" height="auto">
                        <p><strong>Dirección:</strong><br><?php echo $bienes->Direccion; ?></p>
                        <p><strong>Ciudad:</strong><?php echo $bienes->Ciudad; ?></p>
                        <p><strong>Teléfono:</strong><?php echo $bienes->Telefono; ?></p>
                        <p><strong>Código Postal:</strong><?php echo $bienes->Codigo_Postal; ?></p>
                        <p><strong>Tipo:</strong><?php echo $bienes->Tipo; ?></p>
                        <p><strong>Precio:</strong><?php echo $bienes->Precio; ?></p>  
                        <?php 
                            if(isset($listado)){?>
                                <input type="hidden" name="direccion" value="<?php echo $bienes->Direccion;?>">
                                <input type="hidden" name="ciudad" value="<?php echo $bienes->Ciudad;?>">
                                <input type="hidden" name="telefono" value="<?php echo $bienes->Telefono;?>">
                                <input type="hidden" name="codigo_postal" value="<?php echo $bienes->Codigo_Postal;?>">
                                <input type="hidden" name="tipo" value="<?php echo $bienes->Tipo;?>">
                                <input type="hidden" name="precio" value="<?php echo $bienes->Precio;?>">
                                <input type="submit" class="btn green" value="Guardar" id="submitButton">
                        <?php } ?>
                    </form>
                </div> <?php
            }?>    
          </div>
        </div>
      </div>
    </div>


    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    
    <script type="text/javascript" src="js/ion.rangeSlider.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/buscador.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
          $( "#tabs" ).tabs();
      });
    </script>
  </body>
  </html>
