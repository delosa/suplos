<div class="colContenido" id="divReportes">
    <div class="tituloContenido card" style="justify-content: center;">
        <h5>Exportar Reporte:</h5>
        <div class="divider"></div>
        <div class="colFiltros">
            <form action="#" method="post" id="formulario">
                <div class="filtrosContenido">
                <div class="tituloFiltros">
                    <h5>Filtros</h5>
                </div>
                <div class="filtroCiudad input-field">
                    <p><label for="selectCiudad">Ciudad:</label><br></p>
                    <select name="ciudad" id="selectCiudad">
                    <option value="">Elige una ciudad</option>
                    <?php
                        foreach($ciudades as $ciudad => $value){
                            if($_POST['ciudad'] && $ciudad == $_POST['ciudad']){ ?>
                                <option value="<?php echo $ciudad;?>" selected><?php echo $ciudad;?></option>
                                <?php
                            }
                            else{ ?>
                                <option value="<?php echo $ciudad;?>"><?php echo $ciudad;?></option>
                            <?php
                            }
                            ?>
                            <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="filtroTipo input-field">
                    <p><label for="selecTipo">Tipo:</label></p>
                    <br>
                    <select name="tipo" id="selectTipo">
                    <option value="">Elige un tipo</option>
                    <?php
                        foreach($tipos as $tipo => $value){
                            if($_POST['tipo'] && $tipo == $_POST['tipo']){ ?>
                                <option value="<?php echo $tipo;?>" selected><?php echo $tipo;?></option>
                                <?php
                            }
                            else{ ?>
                                <option value="<?php echo $tipo;?>"><?php echo $tipo;?></option>
                            <?php
                            }
                            ?>
                            <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="filtroPrecio">
                    <label for="rangoPrecio">Precio:</label>
                    <input type="text" id="rangoPrecio" name="precio" value="" />
                </div>
                <div class="botonField">
                    <input type="hidden" value="1" name="exportar"> 
                    <input type="submit" class="btn white" value="GENERAR EXCEL" id="submitButton">
                </div>
                </div>
            </form>
            </div>
    </div>
</div>