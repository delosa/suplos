<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body style="display:none;">

<h2>Lista para Exportar Bienes Raices</h2>

<table id="exportar">
  <tr>
    <th>ID</th>
    <th>Dirección</th>
    <th>Ciudad</th>
    <th>Teléfono</th>
    <th>Código Postal</th>
    <th>Tipo</th>
    <th>Precio</th>
  </tr>
  <?php
    foreach ($exportar as $bienes){ ?>
      <tr>
        <td><?php echo $bienes['id'];?></td>
        <td><?php echo $bienes['direccion'];?></td>
        <td><?php echo $bienes['ciudad'];?></td>
        <td><?php echo $bienes['telefono'];?></td>
        <td><?php echo $bienes['codigo_postal'];?></td>
        <td><?php echo $bienes['tipo'];?></td>
        <td><?php echo $bienes['precio'];?></td>
      </tr>
        <?php }
  ?>
  
</table>
</body>
</html>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.1/xlsx.full.min.js"></script>
<script>
    setTimeout('ExportExcel()',500);
    setTimeout(function () {
        window.location.replace("index.php");
        }, 700);

    function ExportExcel(type, fn, dl) {
        var fileName = $('h2').text();
        var elt = document.getElementById('exportar');
        var wb = XLSX.utils.table_to_book(elt, {sheet:"Mi lista de bienes"});
        return dl ?
        XLSX.write(wb, {bookType:type, bookSST:true, type: 'base64'}) :
        XLSX.writeFile(wb, fn || (fileName + "." + (type || 'xlsx')));
    }
    
</script>