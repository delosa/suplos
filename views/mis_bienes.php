<div class="colContenido" id="divResultadosBusqueda">
    <div class="tituloContenido card" style="justify-content: center;">
    <h5>Bienes guardados <?php echo count($datos);?>:</h5>
    <div class="divider"></div>
    <?php

    foreach ($datos as $guardados => $bienes){?>
        <div class="bienes_raices">
            <form action="#" method="post" id="<?php echo $bienes->Id; ?>">
                <img src="img/casas.png" width="250" height="auto">
                <p><strong>Dirección:</strong><br><?php echo $bienes['direccion']; ?></p>
                <p><strong>Ciudad:</strong><?php echo $bienes['ciudad']; ?></p>
                <p><strong>Teléfono:</strong><?php echo $bienes['telefono']; ?></p>
                <p><strong>Código Postal:</strong><?php echo $bienes['codigo_postal']; ?></p>
                <p><strong>Tipo:</strong><?php echo $bienes['tipo']; ?></p>
                <p><strong>Precio:</strong><?php echo $bienes['precio']; ?></p> 
                <input type="hidden" name="id" value="<?php echo $bienes['id'];?>">
                <input type="submit" class="btn green" value="Eliminar" id="submitButton"> 
            </form>
        </div>
        <?php
    }?>
</div>
                